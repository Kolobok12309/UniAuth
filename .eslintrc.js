module.exports = {
    root: true,
    env: {
        node: true
    },
    "extends": "airbnb-base",
    rules: {
        indent: [
            'error',
            4
        ],
        "prefer-destructuring": ["error", {
            "array": false,
            "object": true
        }],
        'linebreak-style': ["off", "windows"],
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        // 'no-debugger': 'off',
        'max-len': ["error", { "code": 300 }],
        'no-param-reassign': ["error", { "props": false }],
    }
};